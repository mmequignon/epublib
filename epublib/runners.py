#!/usr/bin/env python3

import os
import logging
import string

from epublib.epub import Epub
from epublib.exceptions import (
    WrongExtensionException, FolderExistsException
)

class CommonRunner:

    @classmethod
    async def set_metadata(cls, in_filename, out_filename, vals):
        epub = await Epub._get_epub(in_filename)
        for key, value in vals.items():
            epub.set_metadata(key, value)
        await epub.write(out_filename)

    @classmethod
    def get_title_from_filename(cls, filename):
        """
        - Remove the file extension
        - Replace underscores by spaces
        - Capitalize the result
        """
        filename = ".".join(filename.split(".")[:-1])
        filename = filename.replace("_", " ")
        filename = string.capwords(filename)
        return filename
        
    @classmethod
    def check_epub(cls, filename):
        return filename.endswith(".epub")

    def format_text(cls, text):
        text = text.lower()
        text = text.replace(" ", "_")
        return text

    def run(self):
        raise NotImplementedError

class OrganizeRunner(CommonRunner):

    def __init__(self, dir_structure, path):
        self.dir_structure = dir_structure
        self.input_path = path
        self.out_folder = path + "-out"
        try:
            os.mkdir(self.out_folder)
        except:
            logging.critical(
                "Folder {} already exists".format(self.out_folder)
            )
            exit(1)
        self.out_files = {}

    async def _add_epub_in_arbo(self, epub, filename):
        folder_arbo = self.out_folder
        for tag in self.dir_structure:
            val = epub.get_metadata(tag) or "Unknown"
            formatted_val = self.format_text(val)
            folder_arbo = os.path.join(folder_arbo, formatted_val)
            try:
                os.mkdir(folder_arbo)
                logging.info("Folder {} created.".format(folder_arbo))
            except:
                pass
        out_filename = os.path.join(folder_arbo, filename)
        await epub.write(out_filename)

    async def run(self):
        """
        In this mode, all files found in the input folder path will
        be organized with respect to the provided folder tree template.
        example:
        epublib --organize "author/genre/year" path/
        in this example, the output folder will be defined given the
        values of the file metadata.
        If a metadata isn't set for a given file, then the created folder
        will be named `undefined`.
        """
        for folder, _, filenames in os.walk(self.input_path):
            for filename in filenames:
                self.check_epub(filename)
                if self.check_epub(filename):
                    fullpath = os.path.join(folder, filename)
                    epub = await Epub._get_epub(fullpath)
                    await self._add_epub_in_arbo(epub, filename)

class RenameRunner(CommonRunner):

    def __init__(self, template, path):
        self.input_path = path
        if os.path.isdir(path):
            self.type = "dir"
            self.out_folder = self.input_path + "-out"
        else:
            if not self.check_epub(input_path):
                print("input file does seems to be a valid epub")
                exit(1)
            self.type = "file"
        self.template = template

    @classmethod
    def _get_metadata(cls, epub):
        """Returns ebook's xml metadata."""
        res = {}
        elems = epub._get_metadatas()
        for elem in elems:
            prefix, has_namespace, suffix = elem.tag.partition("}")
            if has_namespace:
                key = suffix
            else:
                key = prefix
            res[key] = elem.text
        return res

    async def rename(self, input_filepath=None, output_folder=None):
        if input_filepath is None:
            input_filepath = self.input_path
        if output_folder is None:
            output_folder = "/".join(input_filepath.split("/")[:-1])
        epub = await Epub._get_epub(input_filepath)
        metadata_mapping = self._get_metadata(epub)
        filename = self.template
        for k, v in metadata_mapping.items():
            if v:
                filename = filename.replace(k, v)
        filename = filename + ".epub"
        output_filepath = os.path.join(output_folder, filename)
        await epub.write(output_filepath)

    async def recursive_rename(self):
        for folder, _, filenames in os.walk(self.input_path):
            out_folder = folder.replace(self.input_path, self.out_folder)
            if not os.path.exists(out_folder):
                os.mkdir(out_folder)
            for filename in filenames:
                if self.check_epub(filename):
                    fullpath = os.path.join(folder, filename)
                    await self.rename(fullpath, out_folder)

    def run(self):
        """
        In this mode, given a template provided from command line,
        each tag will be replaced by its value, if any.
        example:
        epublib --rename "author_date_title" file.epub
        How it works:
        This script will try to find and replace tag names from the DC namespace
        If a given tag is found in the template but isn't set on the epub,
        then it will be set as `unknown` in the resulting filename.
        """
        if self.type == "dir":
            self.recursive_rename()
        else:
            self.rename()

class SetMetadataFromPathRunner(CommonRunner):

    def __init__(self, tags, path):
        if not os.path.exists(path):
            print("input file or folder {} does not exists".format(path))
            exit(1)
        self.input_path = path
        self.out_folder = self.input_path + "-out"
        self.tags = tags

    async def run(self):
        """
        In this mode, the metadata values are get from folder structure.
        root_folder/val1/val2/title_name.epub
        if you provided "author/date", then it will set metadata as
        author=val1 and date=val2.
        Do not provide the title name, as it is automatically appended to
        the list of tags you provided.
        Also the root folder (which you provided) is ignored. 
        """
        for folder, _, filenames in os.walk(self.input_path):
            out_folder = folder.replace(self.input_path, self.out_folder)
            if not os.path.exists(out_folder):
                os.mkdir(out_folder)
            # Remove the root folder name
            common_tag_vals = out_folder.split("/")[1:]
            common_tag_keys = self.tags[:len(common_tag_vals)]
            for filename in filenames:
                if self.check_epub(filename):
                    prettyfied_title = self.get_title_from_filename(filename)
                    # TODO UGLY
                    file_tag_vals = common_tag_vals + [prettyfied_title]
                    file_tag_keys = common_tag_keys + ["title"]
                    values_mapping = dict(zip(file_tag_keys, file_tag_vals))
                    fullpath = os.path.join(folder, filename)
                    out_path = os.path.join(out_folder, filename)
                    await self.set_metadata(
                        fullpath, out_path, values_mapping
                    )

class SetMetadataRunner(CommonRunner):

    def __init__(self, metadata, path):
        if not os.path.exists(path):
            logging.critical(
                "input file or folder {} does not exists".format(path)
            )
            exit(1)
        self.metadata = metadata
        self.input_path = path
        if os.path.isdir(path):
            self.type = "dir"
            self.out_folder = path + "-out"
            if os.path.exists(self.out_folder):
                raise FolderExistsException(self.out_folder)
        else:
            self.type = "file"
            if not self.check_epub(path):
                raise WrongExtensionException(path)
            self.input_path = path
            filename_elems = path.split(".")[:-1]
            extension = path.split(".")[-1]
            filename_elems.append("-out")
            filename_elems.append(extension)
            self.output_path = ".".join(filename_elems)

    async def recursive_set_metadata(self):
        for folder, _, filenames in os.walk(self.input_path):
            out_folder = folder.replace(self.input_path, self.out_folder)
            if not os.path.exists(out_folder):
                os.mkdir(out_folder)
            for filename in filenames:
                if self.check_epub(filename):
                    in_filename = os.path.join(folder, filename)
                    out_filename = os.path.join(out_folder, filename)
                    await self.set_metadata(
                        in_filename, out_filename, self.metadata
                    )

    async def run(self):
        """
        In this mode, the metadata to be set are defined as a list of
        keys and values as a command line argument.
        example:
        epublib --set-metadata "author=John Doe,title=Foo Bar" folder/
        Depending on the path you provided, those metadata will be set
        on a single file or recursively on all epub files in children
        folders.
        """
        if self.type == "file":
            await self.set_metadata(
                self.input_path, self.output_path, self.metadata
            )
        if self.type == "dir":
            await self.recursive_set_metadata()
