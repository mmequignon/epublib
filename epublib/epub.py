#!/usr/bin/env python3

import asyncio
from sys import argv
import os
import re
import zipfile

from lxml import etree

from epublib.exceptions import InvalidTagException

NAMESPACES = {
    'XML': 'http://www.w3.org/XML/1998/namespace',
    'EPUB': 'http://www.idpf.org/2007/ops',
    'DAISY': 'http://www.daisy.org/z3986/2005/ncx/',
    'OPF': 'http://www.idpf.org/2007/opf',
    'CONTAINERNS': 'urn:oasis:names:tc:opendocument:xmlns:container',
    'DC': 'http://purl.org/dc/elements/1.1/',
    'XHTML': 'http://www.w3.org/1999/xhtml'
}


class Epub:
    """Epub lib which aims to provide tools to edit ebooks metadata."""


    valid_tags = ["creator", "date", "identifier", "title"]
    container_path = 'META-INF/container.xml'

    @classmethod
    async def _get_epub(cls, path):
        epub = cls(path)
        await epub._parse()
        return epub

    def __init__(self, path):
        self.path = path

    async def _parse(self):
        self.memfiles = self._extract(self.path)
        self.opfpath = self._get_opf_path()
        self.metadata = self._get_metadatas()

    def _extract(self, path):
        """Takes an epub path as argument and returns all files
        contained in the corresponding archive as a dict.
        Dict format : {file_path: file_content}"""
        with zipfile.ZipFile(path, "r", zipfile.ZIP_DEFLATED) as archive:
            memfiles = {
                path: archive.read(path) for path in archive.namelist()
            }
        return memfiles

    def _get_opf_path(self):
        """Returns the epub's opf file path."""
        container_file = self.memfiles[self.container_path]
        container = etree.XML(container_file)
        xpath = "//CONTAINERNS:rootfile"
        opf_tag = container.xpath(xpath, namespaces=NAMESPACES)[0] # FIXME : utiliser find()
        return opf_tag.get("full-path")

    def _get_metadatas(self):
        """Returns ebook's xml metadata."""
        xpath = "//OPF:metadata"
        opf_xml = etree.XML(self.memfiles[self.opfpath])
        return opf_xml.xpath(xpath, namespaces=NAMESPACES)[0]# FIXME : utiliser find()

    def _clean_metadata(self):
        """Removes all metadata that are not in the "DC" namespace."""
        dc_regex = re.compile(r"\{%s\}.*" % NAMESPACES["DC"])
        for elem in self.metadata:
            if not dc_regex.match(elem.tag):
                self.metadata.remove(elem)

    def _update_opf(self):
        # TODO : nettoyage, c'est dégueulasse
        """Clean updated metadata and push changes in the opf memory file."""
        self._clean_metadata()
        opf_xml = etree.XML(self.memfiles[self.opfpath])
        metadata_xml = opf_xml.xpath("//OPF:metadata", namespaces=NAMESPACES)[0]# FIXME : utiliser find()
        for elem in metadata_xml:
            metadata_xml.remove(elem)
        metadata_xml.extend(self.metadata.getchildren())
        self.memfiles[self.opfpath] = etree.tostring(opf_xml, pretty_print=True)

    async def write(self, path=None):
        """Replaces or creates a new epub file with updated metadata."""
        self._update_opf()
        if path is None:
            if os.path.exists(self.path):
                os.remove(self.path)
            path = self.path
        with zipfile.ZipFile(path, "w", zipfile.ZIP_DEFLATED) as outzip:
            for filepath, content in self.memfiles.items():
                if filepath == "mimetype":
                    outzip.writestr(
                        filepath, content, compress_type=zipfile.ZIP_STORED)
                else:
                    outzip.writestr(filepath, content)

    def get_metadata(self, tag):
        if tag not in self.valid_tags:
            raise InvalidTagException(tag)
        xpath = "//DC:%s" % tag
        res = self.metadata.xpath(xpath, namespaces=NAMESPACES)
        if res:
            return res[0].text


    def set_metadata(self, tag, value):
        """Updates or set "DC" namespaced metadata. InvalidTagException if
        the name is not valid."""
        if tag not in self.valid_tags:
            raise InvalidTagException(tag)
        xpath = "//DC:%s" % tag
        elem = self.metadata.xpath(xpath, namespaces=NAMESPACES)
        if elem:
            elem = elem[0]
        else:
            elem = etree.Element("{%s}%s" % (NAMESPACES["DC"], tag))
            self.metadata.append(elem)
        elem.text = value

    def remove_metadata(self, name):
        """Removes a metadata given its tag if exists."""
        xpath = "//DC:%s" % name
        elem = self.metadata.xpath(xpath, namespaces=NAMESPACES)
        if elem:
            self.metadata.remove(elem)
