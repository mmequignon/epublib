#!/usr/bin/env python3

class InvalidTagException(Exception):
    """Exception raised when trying to set a wrongly tagged metadata"""

    def __init__(self, tag):
        log = """%s is not defined in the "DC" namespace.""" % tag
        super(InvalidTagException, self).__init__(log)

class WrongExtensionException(Exception):
    """Exception raised when filename does not end with `.epub`"""

    def __init__(self, filename):
        print("File {} has no .epub extension".format(filename))

class FolderExistsException(Exception):

    def __init__(self, path):
        print("Folder {} already exists.".format(path))
