#!/usr/bin/env python3

import asyncio

from epublib.main import EpubLibMain



if __name__ == "__main__":
    main_class = EpubLibMain()
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main_class.run())
