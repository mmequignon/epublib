#!/usr/bin/env python3

import argparse
import logging


from epublib.actions import (
    SetMetadataAction, SetMetatadaFromPathAction, OrganizeAction, PathAction
)
from epublib.runners import (
    SetMetadataRunner, SetMetadataFromPathRunner, RenameRunner, OrganizeRunner
)


logging.basicConfig(
    format='%(levelname)s - %(asctime)s - %(message)s',
    datefmt='%I:%M:%S', level=logging.INFO
)


class EpubLibMain:

    def __init__(self):
        self.args = self._get_args()
        self.runner = self._get_runner()

    def _get_args(self):
        parser = argparse.ArgumentParser(
            description="A Python tool to organize and rename epubs."
        )
        group = parser.add_mutually_exclusive_group(required=True)
        group.add_argument("--set-metadata", action=SetMetadataAction)
        group.add_argument(
            "--set-metadata-from-path", action=SetMetatadaFromPathAction
        )
        group.add_argument("--rename")
        group.add_argument("--organize", action=OrganizeAction)
        parser.add_argument("target", action=PathAction)
        args = vars(parser.parse_args())
        return args

    def _get_runner(self):
        runner = None
        input_path = self.args["target"]
        if self.args.get("set_metadata"):
            arg = self.args["set_metadata"]
            runner = SetMetadataRunner(arg, input_path)
        elif self.args.get("set_metadata_from_path"):
            arg = self.args["set_metadata_from_path"]
            runner = SetMetadataFromPathRunner(arg, input_path)
        elif self.args.get("rename"):
            arg = self.args["rename"]
            runner = RenameRunner(arg, input_path)
        elif self.args.get("organize"):
            arg = self.args["organize"]
            runner = OrganizeRunner(arg, input_path)
        if runner is None:
            logging.critical("No action specified.")
            exit(1)
        return runner

    async def run(self):
        await self.runner.run()
