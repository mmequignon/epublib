#!/usr/bin/env python3

import os

from argparse import Action

from epublib.data import VALID_METADATA_TAGS
from epublib.exceptions import InvalidTagException


class OrganizeAction(Action):

    def __call__(self, parser, namespace, values, option_string=None):
        tags = values.split("/")
        for tag in tags:
            if tag not in VALID_METADATA_TAGS:
                raise InvalidTagException(tag)
        setattr(namespace, self.dest, tags)


class PathAction(Action):
    def __call__(self, parser, namespace, values, option_string=None):
        if not os.path.exists(values):
            raise OSError
        setattr(namespace, self.dest, values.rstrip("/"))


class SetMetadataAction(Action):

    def __call__(self, parser, namespace, values, option_string=None):
        args = values.split(",")
        res = {}
        for arg in args:
            tag, value = arg.split("=")
            if tag not in VALID_METADATA_TAGS:
                raise InvalidTagException(tag)
            res[tag] = value
        setattr(namespace, self.dest, res)

class SetMetatadaFromPathAction(Action):

    def __call__(self, parser, namespace, values, option_string=None):
        tags = []
        for tagname in values.split("/"):
            if tagname not in VALID_METADATA_TAGS:
                raise InvalidTagException(tag)
            if tagname == "title": # title is already handled
                continue
            tags.append(tagname)
        setattr(namespace, self.dest, tags)
