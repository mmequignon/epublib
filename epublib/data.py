#!/usr/bin/env python3

VALID_METADATA_TAGS = [
    "creator", "date"
]
