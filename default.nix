with import <nixpkgs> {};
with python3Packages;

buildPythonPackage rec {
  name = "epublib";
  src = ./.;
  propagatedBuildInputs = [lxml];
}
