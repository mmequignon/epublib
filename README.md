# YAOURT

Yet Another Organizing/Updater/Renaming Tool (for epubs)

## Usages

<b>General note</b>: For most of the available commands, a filename or a folder name can be provided.
- If a file name is provided, then the command will be applied to the file.
- If a folder name is provided, then the command will be applied recursively on the given folder.

### Set MetaData

Set epub metadatas.

The list of metadata should be provided in a comma-separated key=value format.

Example:
```python
epublib --set-metadata "author=foo-bar,year=1970" <folder/filename>
```

### Rename

Creates renamed copies of the provided filename or foldername.

The output filenames will be generated according to the provided filename format and files metadata.

Example:
```python
epublib --rename "year-author-title" <folder/filename> 
```

### Set MetaData from Path

<b>/!\\ folder only command</b>

Sets metadata given a provided tree structure description.

The tree structure description must be provided as a list of tags separated by slashes.

<b>Note</b>: As the title is generally the filename, it will be automatically added to the list.
Any occurence in the provided tree description will be dropped.

Example:
```python
epublib --set-metadata-from-path "author/date" <folder>
```

### Organize

<b>/!\\ folder only command</b>

Creates a new output folder next to the folder provided as argument.
Its structure will be organized with respect to the provided folder tree description.
This description must be a `slashed` separated metadata names.

Example:
```
epublib -R --organize "author/year" <folder> 
[...]
tree epubs-out
epubs-out
├── alexandre_dumas
│   └── 2017-03-15
│       ├── Dumas, Alexandre - Le comte de Monte-Cristo.epub
│       └── Dumas, Alexandre - Les trois mousquetaires.epub
├── jules_verne
│   └── 2017-03-15
│       ├── Verne, Jules - Cinq Semaines en Ballon.epub
│       ├── Verne, Jules - Le tour du monde en quatre-vingts jours.epub
│       └── Verne, Jules - L'ile mysterieuse .epub
└── stendhal
    └── 2017-03-15
        └── Stendhal - Le Rouge et le Noir.epub
```

## Notes

For a list of valid metadata, refer to the [dc](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#section-3) namespace.
